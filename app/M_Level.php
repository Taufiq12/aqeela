<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_Level extends Model
{
    protected $table = 'm_level';
    protected $fillable = ['status'];
}
