<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class M_Type_Member extends Model
{
    protected $table = 'm_type_member';
    protected $fillable = ['type'];
}
