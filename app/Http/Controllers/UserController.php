<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'status' => 400,
                    'message' => 'invalid_credentials'
                ], 400);
            }
        } catch (JWTException $e) {
            return response()->json([
                'status' => 500,
                'message' => 'could_not_create_token'
            ], 500);
        }

        return response()->json([
            'status' => 200,
            'token' => $token
        ], 200);

    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if($validator->fails()){
            return response()->json([
                'status' => 400,
                'message' => $validator->errors()->toJson()
            ], 400);
        }

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);

        $token = JWTAuth::fromUser($user);

        return response()->json([
            'status' => 200,
            'message' => 'success',
            'user' => $user,
            'token' => $token
        ], 200);
    }


    public function update($id, Request $request)
    {
        $user = User::find($id);
        $user->update($request->all());

        return response()->json([
            'status' => 200,
            'message' => 'success update',
            'data' => $user
        ], 200);

    }

    public function delete($id)
    {
        $user = Person::find($id);
        $user->delete();

        return response()->json([
            'status' => 204,
            'message' => 'success delete'
        ], 204);
    }

    public function getAuthenticatedUser()
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json([
                    'status' =>400,
                    'message' => 'user_not_found'
                ], 400);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json([
                'status' => 4001,
                'message' => 'token_expired'
            ], 401);

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json([
                'status' => 400,
                'message' => 'token_invalid'
            ], 400);

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json([
                'status' => 401,
                'message' => 'token_absent'
            ], 401);

        }

        return response()->json(compact('user'));
    }
}
