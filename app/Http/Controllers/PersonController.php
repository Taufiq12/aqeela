<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// import file model Person
use App\Person;

class PersonController extends Controller
{
    // mengambil semua data
    public function all()
    {
        return Person::all();
    }

    // mengambil data by id
    public function show($id)
    {
        return Person::find($id);
    }

    // menambah data
    public function store(Request $request)
    {
        return Person::create($request->all());
    }

    // mengubah data
    public function update($id, Request $request)
    {
        $Person = Person::find($id);
        $Person->update($request->all());
        return $Person;
    }

    // menghapus data
    public function delete($id)
    {
        $Person = Person::find($id);
        $Person->delete();
        return 204;
    }
}
