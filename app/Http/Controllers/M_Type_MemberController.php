<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// import file model M_Type_Member
use App\M_Type_Member;

class M_Type_MemberController extends Controller
{
    // mengambil semua data
    public function all()
    {
        return M_Type_Member::all();
    }

    // mengambil data by id
    public function show($id)
    {
        return M_Type_Member::find($id);
    }

    // menambah data
    public function create(Request $request)
    {
        $M_Type_Member = M_Type_Member::create([
            'type' => $request->get('type'),
        ]);

        return response()->json([
            'status' => 200,
            'message' => 'success register',
            'data' => $M_Type_Member,
        ], 200);
    }

    // mengubah data
    public function update($id, Request $request)
    {
        $M_Type_Member = M_Type_Member::find($id);
        $M_Type_Member->update($request->all());

        return response()->json([
            'status' => 200,
            'message' => 'success update',
            'data' => $M_Type_Member
        ], 200);
    }

    // menghapus data
    public function delete($id)
    {
        $M_Type_Member = M_Type_Member::find($id);
        $M_Type_Member->delete();

        return response()->json([
            'status' => 204,
            'message' => 'success delete'
        ], 204);
    }
}
