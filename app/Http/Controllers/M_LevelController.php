<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// import file model M_Level
use App\M_Level;

class M_LevelController extends Controller
{
    // mengambil semua data
    public function all()
    {
        return M_Level::all();
    }

    // mengambil data by id
    public function show($id)
    {
        return M_Level::find($id);
    }

    // menambah data
    public function create(Request $request)
    {
        $M_Level = M_Level::create([
            'status' => $request->get('status'),
        ]);

        return response()->json([
            'status' => 200,
            'message' => 'success',
            'data' => $M_Level,
        ], 200);
    }

    // mengubah data
    public function update($id, Request $request)
    {
        $M_Level = M_Level::find($id);
        $M_Level->update($request->all());

        return response()->json([
            'status' => 200,
            'message' => 'success update',
            'data' => $M_Level
        ], 200);
    }

    // menghapus data
    public function delete($id)
    {
        $M_Level = M_Level::find($id);
        $M_Level->delete();

        return response()->json([
            'status' => 204,
            'message' => 'success delete'
        ], 204);
    }
}
