<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

// import file model Member
use App\Member;

class MemberController extends Controller
{
    // mengambil semua data
    public function all()
    {
        return $response=Member::select('id_type','name','no_identity','dateofbirth','address','mobile_phone','phone_number','email')->get();
    }

    // mengambil data by id
    public function show($id)
    {
        return $response=Member::select('id_type','name','no_identity','dateofbirth','address','mobile_phone','phone_number','email')->where('id', '=', $id)->get();
    }

    // menambah data
    public function create(Request $request)
    {
        $Member = Member::create([
            'id_type' => $request->get('id_type'),
            'name' => $request->get('name'),
            'no_identity' => $request->get('no_identity'),
            'dateofbirth' => $request->get('dateofbirth'),
            'address' => $request->get('address'),
            'mobile_phone' => $request->get('mobile_phone'),
            'phone_number' => $request->get('phone_number'),
            'email' => $request->get('email'),
            'password' =>Hash::make($request->get('password'))

        ]);


        return response()->json([
            'status' => 200,
            'message' => 'success',
            'data' => $Member,
        ], 200);
    }

    // mengubah data
    public function update($id, Request $request)
    {
        $Member = Member::find($id);
        $Member->update($request->all());

        return response()->json([
            'status' => 200,
            'message' => 'success update',
            'data' => $Member
        ], 200);
    }

    // menghapus data
    public function delete($id)
    {
        $Member = Member::find($id);
        $Member->delete();

        return response()->json([
            'status' => 204,
            'message' => 'success delete'
        ], 204);
    }
}
