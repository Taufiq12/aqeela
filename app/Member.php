<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table = 'member';
    protected $fillable = ['id_type','name','no_identity','dateofbirth','address','mobile_phone','phone_number','email','password'];
}
