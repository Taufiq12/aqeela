<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Platform Pembiayaan Syariah | Aqeela</title>
    <meta name="description" content="Platform Pembiayaan Syariah">
    <meta name="keywords" content="Pembiayaan Syariah">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link href="{{ mix('css/app.css') }}" type="text/css" rel="stylesheet" />
    <script src="{{ mix('js/app.js') }}" type="text/javascript" defer></script>

    <!-- favicon -->
		<link rel="shortcut icon" type="image/x-icon" href="img/logo/aqeela.ico">
		<!-- all css here -->

		<!-- bootstrap v3.3.6 css -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- owl.carousel css -->
		<link rel="stylesheet" href="css/owl.carousel.css">
		<link rel="stylesheet" href="css/owl.transitions.css">
        <!-- meanmenu css -->
        <link rel="stylesheet" href="css/meanmenu.min.css">
		<!-- font-awesome css -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/icon.css">
		<link rel="stylesheet" href="css/flaticon.css">
		<!-- magnific css -->
        <link rel="stylesheet" href="css/magnific.min.css">
		<!-- venobox css -->
		<link rel="stylesheet" href="css/venobox.css">
		<!-- style css -->
		<link rel="stylesheet" href="style.css">
		<!-- responsive css -->
		<link rel="stylesheet" href="css/responsive.css">

		<script src="js/vendor/jquery-1.12.4.min.js" type="text/javascript" defer></script>
		<script src="js/bootstrap.min.js" type="text/javascript" defer></script>
		<script src="js/owl.carousel.min.js" type="text/javascript" defer></script>
		<script src="js/jquery.counterup.min.js" type="text/javascript" defer></script>
		<script src="js/waypoints.js" type="text/javascript" defer></script>
        <script src="js/jquery.stellar.min.js" type="text/javascript" defer></script>

        <script src="js/magnific.min.js" type="text/javascript" defer></script>
		<script src="js/venobox.min.js" type="text/javascript" defer></script>
        <script src="js/jquery.meanmenu.js" type="text/javascript" defer></script>
		<script src="js/form-validator.min.js" type="text/javascript" defer></script>
		<script src="js/plugins.js" type="text/javascript" defer></script>
		<script src="js/main.js" type="text/javascript" defer></script>

</head>

<body style='background-color: white'>
    <div id="app">
    </div>
</body>

</html>
