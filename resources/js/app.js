require('./bootstrap');

window.Vue = require('vue');

// import dependecies tambahan
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import Axios from 'axios';

Vue.use(VueRouter,VueAxios,Axios);

// import file yang dibuat tadi
import App from './components/App.vue';
import Home from './components/Home.vue';
import Contact from './components/Contact.vue';
import Create from './components/Create.vue';
import Read from './components/Read.vue';
import Update from './components/Update.vue';
import FAQ from './components/FAQ.vue';
import MemilihKami from './components/MemilihKami.vue';
import ResikoPendanaan from './components/ResikoPendanaan.vue';
import Mukadimah from './components/Mukadimah.vue';

// membuat router
const routes = [
    {
        name: 'home',
        path: '/',
        component: Home
    },
    {
        name: 'contact',
        path: '/',
        component: Contact
    },
    {
        name: 'faq',
        path: '/',
        component: FAQ
    },
    {
        name: 'memilih-kami',
        path: '/',
        component: MemilihKami
    },
    {
        name: 'resiko-pendanaan',
        path: '/',
        component: ResikoPendanaan
    },
    {
        name: 'mukadimah',
        path: '/',
        component: Mukadimah
    },
    {
        name: 'read',
        path: '/',
        component: Read
    },
    {
        name: 'create',
        path: '/create',
        component: Create
    },
    {
        name: 'update',
        path: '/detail/:id',
        component: Update
    }
]

const router = new VueRouter({ mode: 'history', routes: routes });
new Vue(Vue.util.extend({ router }, App)).$mount("#app");
