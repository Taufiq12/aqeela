<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//API Person
Route::get('/person','PersonController@all');
Route::get('/person/{id}','PersonController@show');
Route::post('/person','PersonController@store');
Route::put('/person/{id}','PersonController@update');
Route::delete('/person/{id}','PersonController@delete');

//API Register
Route::post('register', 'UserController@register');
Route::put('/register/{id}','UserController@update')->middleware('jwt.verify');
Route::delete('/register/{id}','UserController@delete')->middleware('jwt.verify');

//API Login
Route::post('login', 'UserController@login');

//API Master Level
Route::post('m_level', 'M_LevelController@create')->middleware('jwt.verify');
Route::put('/m_level/{id}','M_LevelController@update')->middleware('jwt.verify');
Route::delete('/m_level/{id}','M_LevelController@delete')->middleware('jwt.verify');
Route::get('/m_level','M_LevelController@all')->middleware('jwt.verify');
Route::get('/m_level/{id}','M_LevelController@show')->middleware('jwt.verify');

//API Master Jenis Member
Route::post('m_type_member', 'M_Type_MemberController@create')->middleware('jwt.verify');
Route::put('/m_type_member/{id}','M_Type_MemberController@update')->middleware('jwt.verify');
Route::delete('/m_type_member/{id}','M_Type_MemberController@delete')->middleware('jwt.verify');
Route::get('/m_type_member','M_Type_MemberController@all')->middleware('jwt.verify');
Route::get('/m_type_member/{id}','M_Type_MemberController@show')->middleware('jwt.verify');

//API Member
Route::post('member', 'MemberController@create')->middleware('jwt.verify');
Route::put('/member/{id}','MemberController@update')->middleware('jwt.verify');
Route::delete('/member/{id}','MemberController@delete')->middleware('jwt.verify');
Route::get('/member','MemberController@all')->middleware('jwt.verify');
Route::get('/member/{id}','MemberController@show')->middleware('jwt.verify');

//Check Token
Route::get('bookall', 'BookController@bookAuth')->middleware('jwt.verify');
Route::get('user', 'UserController@getAuthenticatedUser')->middleware('jwt.verify');
